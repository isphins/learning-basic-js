

//break
for (var i = 0; i < 10; i++) {
  if(i == 5 ){
    break;
  }
  document.write( i + "<br/>");
}
//0 1 2 3 4 

//continue
for (var i = 0; i <= 10; i++) {
    if(i == 5){
      continue;
    }
    document.write( i + "<br />");
}
//0 1 2 3 4 6 7 8 9 10
