Object Properties

```javascript

objectName.propertyName
//
objectName['propertyName']
```

```javascript
var person = {
  name : "John" , age : 31 ,
  favColor : "green",height : 183
}

var x = person.age;
var y = person['age'];


var course = { name : "JS" , lessons : 41};
document.write(course.name.length);
//output 2
```


Object methods


objectName.methodName();


```javascript
document.write("This is a book");
```

Methods are functions that are stored as object properties.
