

## The Object Constructor

### object Constructor function

```javascript
function person(name,age,color){
  this.name = name ;
  this.age = age;
  this.favColor = color;
}
```

## Creating Object

```javascript
var p1 = new person("John" ,42 , "green");
var p2 = new person("Amy", 21 , "red");

document.write(p1.age); // output 42
document.write(p2.name); // output Amy


function person(name,age){
  this.name = name ;
  this.age =age ;
}

var John = new person("John",25);
var James = new person("James" , 21);
```
