
methods

```
methodName : function(){ code line }

objectName.methodName()
```

example

```javascript

function person(name,age){
  this.name = name ;
  this.age =age ;
  this.changeName = function(name){
    this.name = name ;
  }
}

var p = new person("Devid" , 21);
p.changeName("John");
//Now p.name == John
```


## methods

```javascript

function person(name,age){
  this.name = name ;
  this.age =age ;
  this.yearOfBirth = bornYear ;
}

function bornYear(){
  return 2016 - this.age;
}

var p = new person("A",22);
document.write(p.yearOfBirth());
//output 1994
```
