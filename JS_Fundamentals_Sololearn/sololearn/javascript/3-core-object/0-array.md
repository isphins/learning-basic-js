
## Javscript arrays

```Javascript
var course1 = "HTML";
var course2 = "CSS";
var course3 = "JS" ;


var course = new Array("HTML","CSS","JS");
```

## Accessing an arrays

```javascript
var course = new Array("HTML","CSS","JS");

var course = course[0];
course[1] = "C++"; //change the second element



document.write(course[10]);
//output undefined
```
