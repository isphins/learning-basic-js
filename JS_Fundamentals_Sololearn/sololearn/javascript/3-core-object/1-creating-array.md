
## Creating arrays

```javascript

//constructor
var courses = new Array(3);
courses[0] = "HTML";
courses[1] = "CSS";
courses[2] = "JS";



//Array Literal
var course = ["HTML", "CSS","JS"];
```
