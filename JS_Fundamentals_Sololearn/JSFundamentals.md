
JavaScript & DOM Fundamentals

- Builtin methods: Learn the standard data types (especially arrays, objects, strings, and numbers).

- Functions & pure functions: You probably think you’ve got a great grasp of functions, but JavaScript has some tricks up its sleeves, and you’ll need to learn about pure functions to get a handle on functional programming.

- Closures: Learn how JavaScript’s function scopes behave.

- Callbacks: A callback is a function used by another function to signal when there is a result ready. You say, “do your job, call me when it’s done.”

- Promises: A promise is a way to deal with future values. When a function returns a promise, you can attach callbacks using the ``.then()`` method to run after the promise resolves. The resolved value is passed into your callback function, e.g., ``doSomething().then(value => console.log(value));``

```javascript
const doSomething = (err) => new Promise((resolve, reject) => {
  if (err) return reject(err);

  setTimeout(() => {
    resolve(42);
  }, 1000);
});

const log = value => console.log(value);

// Using returned promises
doSomething().then(
  // on resolve:
  log, // logs 42
  // on reject (not called this time)
  log
);

// remember to handle errors!
doSomething(new Error('oops'))
  .then(log) // not called this time
.catch(log); // logs 'Error: oops'
```

- Ajax & server API calls: Most interesting apps eventually need to talk to the network. You should know how to communicate with APIs.

- ES6: The current version of JavaScript is ES2016 (aka ES7), but a lot of developers still haven’t properly learned ES6. It’s past time.

- Classes (note: Avoid class inheritance. See How to Use Classes and Sleep at Night.)

- Functional programming basics: Functional programming produces programs by composing mathematical functions, avoiding shared state & mutable data. It’s been years since I’ve seen a production JavaScript app that didn’t make heavy use of functional programming. It’s time to master the fundamentals.

- Generators & async/await: In my opinion, the best way to write asynchronous code that looks synchronous. It has a learning curve, but once you’ve learned it, the code will be easier to read.

- Performance: RAIL — Start with “PageSpeed Insights” & “WebPageTest.org”

- Progressive Web Applications (PWAs): See “Native Apps are Doomed” & “Why Native Apps Really Are Doomed”

- Node & Express: Node lets you use JavaScript on the server, meaning your users can store data in the cloud and access it anywhere. Express is the most popular framework for Node by a landslide.

- Lodash: A great, modular utility belt for JavaScript, packed with functional programming goodies. Import the data-last functional modules from lodash/fp
