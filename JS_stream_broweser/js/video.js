(function() {
    var video = document.getElementById('video'),
    vendorUrl = window.URL || window.webkitURL;

    navigator.getMedia = navigator.getUserMedia || navigator.WebkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
    //
    navigator.getMedia({
        video: true,
        audio: false
    }, function(stream) {
        video.src = vendorUrl.createObjectURL(stream);
        video.play();
    }, function(error) {
        // An error occured
        // error.
    });

})();

//https://www.codecourse.com/lessons/stream-your-webcam-in-your-browser-with-webrtc
